# Dirty Jobs

![QWAN](http://www.qwan.eu/img/qwan_logo_horizontal.png)

© [QWAN](http://www.qwan.eu/) Quality Without A Name

## Introduction
We will be working on legacy code, and want to make safe changes using Test Driven Development. The system we are working on was not made that way, and we need to find a way to start with our first tests.

## The system
The system is based on actual code we ran into during one of our coaching assignments We simplified it a little and 'depersonalised' it. We are working on a system that tracks vehicles in a network.

![System](doc/system.png)

The field containing trackable vehicles are somewhere on the network and the tracker we'll be working on can request vehicles to send information on their location, speed, direction and such. Vehicles send that information through a proprietary protocol. And our tracker decodes the messages and puts it in a cache. The current vehicle positions are displayed. See for more information on the protocol: Appendix A.

## Setting up the exercise

1. Clone the repository
```bash
git clone https://mgryszko@bitbucket.org/mgryszko/dirtyjobs.git
```

2. Build the project jar
```bash
./gradlew build
```

3. Fire up the RMI registry

Some communication is done through RMI (Remote Method Invocation). Therefore before starting anything else, you'll have to fire up the registry.
```bash
./gradlew rmiregistry
```
**Keep it running.**

4. Run the vehicle field
```bash
./gradlew runVehicleField
```

You should see something like

![Vehicle field](doc/vehicle_field.png)

**Keep it running.**

5. Run the tracker
```bash
./gradlew runTracker
```

You should see something like

![Tracker field](doc/tracker_field.png)

The tracker tracks three vehicles. You will see another field with three squares and magically on the vehicle field three squares have a different color.

**Keep it running.**

`4444` port is used by default. You can change it to any other free port on your machine by editing `build.gradle`.

## Looking at the code
We will be working on the tracker. We need to make changes and sadly there are no tests. The picture below shows an overview of the tracker:

![Class diagram](doc/class_diagram.png)

First we'll study the code and collect your remarks on the code. Don't be polite ;-).

Please focus on the classes

* `VehicleTrackingService`
* `VehicleMessageDecoder`

We'll be working on those later.

## First story

> As a tracker I want the same color as on the big screen to be displayed when tracking vehicle

Vehicles take on another color when they are tracked. The story is about taking on the same color in the tracker field. Oh – eh the color is random.

Hint: The color is returned by the trackVehicle message:
```java
public interface TrackableField extends Remote {
 /**
  * ask a trackable gently to track a vehicle.
  *
  * If the request is accepted the method returns a color that
  * vehicle takes on and the vehicle will start sending info
  * using the protocol defined in Protocol and
  * CodecUtils to the specified host and port.
  *
  * All messages will contain the identification byte passed here.
  *
  * @param host the host to send protocol messages to
  * @param port the port to send protocol messages to
  * @param identification
  * @return the rgb value of the color the sending vehicle takes on.
  * @throws RemoteException
  */
  public abstract Integer trackVehicle(String host, int port, byte identification) throws RemoteException;

}
```

**The rule of the game: NO CODE WITHOUT TESTS!**

## Second story

> As a tracker I want the tracked vehicles position extrapolated when a vehicle discontinues sending information.

Vehicles are buggy in in this version. Sometimes they stop sending messages. The tracker should extrapolate positions at that point.

## Appendix A - the protocol
Protocol contains data in a compressed data stream containing Full and Partial frames
More specifically: repeatedly one full frame is sent and then 4 partal frames:
```
|F|P|P|P|P

It is up to the receiver to assemble full frames based on partials

Full Frame
Fixed length 15 bytes
|F|CRC|I|T|P|D|S|E|A|C|
F Frametype         [1 bytes 0x00 = Full Frame]
CRC                 [1 bytes crc32]
T TimeStamp         [4 bytes unsigned long int Little Endian]
I Identification    [1 byte identification]
P Position          [4 bytes |X|Y| small int x, small int y]]
D Direction         [2 bytes [X|Y]
                              0|1 = south,
                              0|FF = north,
                              1|0 = east,
                             FF|0 = west]
S Speed	            [1 bytes byte value]
E EngineOn          [1 bytes byte value]
A Dangerouslyclose  [1 bytes boolean (1 = true, 0 = false)]
C Collision         [1 bytes boolean (1 = true, 0 = false)]

Partial Frame
Variable length 5 bytes + data (length depends on Frametype - see above)
|F|CRC|I|T|DDDD

F Frametype         [1 bytes 0x01 = Position]
                            [0x02 = Direction]
                            [0x03 = Speed]
                            [0x04 = EngineOn]
                            [0x05 = Dangerouslyclose]
                            [0x06 = Collision]
T Timestamp
I Identification
D data (max 4 bytes)
```

## Appendix B - protocol utilities

`CodecUtils.createFullFrame`

```java
public static byte[] createFullFrame(byte ident,
                                     int tstamp,
                                     int xpos,
                                     int ypos,
                                     Direction dir,
                                     Speed spd,
                                     Engine eon,
                                     CloseToOther close,
                                     Collision coll)
```
Creates a full frame to be send to the tracker. Frame is a byte array with the format as specified in Appendix A.



