package net.xpday.dirtyjobs.tracker;

import net.xpday.dirtyjobs.lib.*;
import net.xpday.dirtyjobs.tracker.TrackingCache.Entry;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class FirstStoryTest {
    @Test
    public void paints_vehicle_in_the_same_color_as_on_the_field() throws Exception {
        fieldReturnsVehicleInGreenColor();

        handleAnyMessage(createDecoder());

        lastTrackingEntryColorIsGreen();
    }

    private VehicleTrackingService trackingService = mock(VehicleTrackingService.class);

    private final byte decoderId = 1;
    private final int green = 0x00FF00;
    private final byte[] anyMessage = anyMessageToDecoder();

    private byte[] anyMessageToDecoder() {
        int anyTimestamp = 0, anyXPos = 0, anyYPos = 0;
        Direction anyDirection = Direction.East;
        Speed anySpeed = Speed.Nul;
        Engine anyEngineStatus = Engine.Off;
        CloseToOther anyProximity = CloseToOther.No;
        Collision anyCollision = Collision.No;
        return CodecUtils.createFullFrame(
            decoderId,
            anyTimestamp,
            anyXPos, anyYPos, anyDirection, anySpeed,
            anyEngineStatus,
            anyProximity,
            anyCollision);
    }

    private void fieldReturnsVehicleInGreenColor() throws Exception {
        when(trackingService.trackVehicle(anyString(), any(MessageDecoder.class))).thenReturn(green);
    }

    private MessageDecoder createDecoder() throws Exception {
        String anyHost = "localhost";
        return new VehicleMessageDecoder(trackingService, anyHost, decoderId);
    }

    private void handleAnyMessage(MessageDecoder decoder) {
        decoder.handleMessage(decoderId, anyMessage, anyMessage.length);
    }

    private void lastTrackingEntryColorIsGreen() {
        assertThat(lastTrackingEntry().getColor(), equalTo(green));
    }

    private Entry lastTrackingEntry() {
        return TrackingCache.getInstance().lastEntry(decoderId);
    }
}
