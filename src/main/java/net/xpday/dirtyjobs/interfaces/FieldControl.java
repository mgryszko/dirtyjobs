/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FieldControl extends Remote {
	/**
	 * Stops vehicles to send messages to their trackers
	 * @throws RemoteException
	 */
	public abstract void stopUpdates() throws RemoteException;

	/**
	 * Resumes vehicles to send messages to their trackers
	 * @throws RemoteException
	 */
	public abstract void resumeUpdates() throws RemoteException;

}