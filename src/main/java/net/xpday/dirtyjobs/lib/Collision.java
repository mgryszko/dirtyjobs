/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

public class Collision {

	private boolean collided;

	public Collision(boolean b) {
		collided = b;
	}

	public static final Collision No = new Collision(false);
	public static final Collision Yes = new Collision(true);

	@Override
	public String toString() {
		return collided ? "Collided" : "Not Collided";
	}
}
