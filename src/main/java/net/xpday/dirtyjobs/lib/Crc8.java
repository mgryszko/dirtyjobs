/**
 * Dirty Jobs workshop code
 *  
 * Found this class and adapted somewhere on the internet 
 */
package net.xpday.dirtyjobs.lib;

/**
 * Crc 8 creates a checksum over a byte array
 */
public class Crc8 {
	/**
	 * return a checksum over the part of data specified by from and length
	 * 
	 * @param data
	 * @param from
	 * @param length
	 * @return the resulting checksum
	 */
	public static byte checksum(byte[] data, int from, int length) {
		short _register = 0;
		short bitMask = 0;
		short poly = 0;
		_register = data[from];

		for (int i = 1; i < length; i++) {
			_register = (short) ((_register << 8) | (data[from+i] & 0x00ff));
			poly = (short) (0x0107 << 7);
			bitMask = (short) 0x8000;

			while (bitMask != 0x0080) {
				if ((_register & bitMask) != 0) {
					_register ^= poly;
				}
				poly = (short) ((poly & 0x0000ffff) >>> 1);
				bitMask = (short) ((bitMask & 0x0000ffff) >>> 1);
			} // end while
		} // end for
		return (byte) _register;
	}

	public static byte checksum(byte[] data) {
		return checksum(data, 0, data.length);
	}
}
