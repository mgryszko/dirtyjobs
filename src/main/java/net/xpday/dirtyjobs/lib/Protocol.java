/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/**
 * Protocol constant definitions
 */
public interface Protocol {

	byte FullFrame = 0x00;
	int FullFrameLength = 18;

	byte PartialPositionFrame = 0x11;
	int PartialPositionFrameLength = 12;

	byte PartialDirectionFrame = 0x12;
	int PartialDirectionFrameLength = 10;

	byte PartialSpeedFrame = 0x13;
	int PartialSpeedFrameLength = 9;
	
	byte PartialEngineFrame = 0x14;
	int PartialEngineFrameLength = 9;
	
	byte PartialClosingFrame = 0x15;
	int PartialClosingFrameLength = 9;
	
	byte PartialCollisionFrame = 0x16;
	int PartialCollisionFrameLength = 9;

	int FrameTypeIndex = 0;
	int CrcIndex = 1;
	int FrameLengthIndex = 2;
	int PayloadIndex = 3;
	int FullFramePayload = 14;
	int IdentificationIndex = 3;
	int TimeStampIndex = 4;
	int XPosIndex = 8;
	int YPosIndex = 10;
	int DirectionIndex = 12;
	int SpeedIndex = 14;
	int EngineIndex = 15;
	int ClosingIndex = 16;
	int CollisionIndex = 17;
	int XPosIndexInDelta = 8;
	int YPosIndexInDelta = 10;
	int DirectionIndexInDelta = 8;
	int SpeedIndexInDelta = 8;
	int EngineIndexInDelta = 8;
	int ClosingIndexInDelta = 8;
	int CollisionIndexInDelta = 8;

}
